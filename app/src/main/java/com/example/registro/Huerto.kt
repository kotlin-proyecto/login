package com.example.registro

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.compose.*
import androidx.navigation.compose.rememberNavController
import com.example.registro.ui.theme.RegistroTheme

class Huerto : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            RegistroTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    pruebaNavigation()
                }
            }
        }
    }
}

@Composable
fun Greeting(navController:NavController) {
    var cont by remember {
        mutableStateOf(0)
    }
    var num by remember {
        mutableStateOf(1)
    }
    var suma by remember {
        mutableStateOf(0)
    }

    var idImagen by remember {
        mutableStateOf(R.drawable.huerto_vacio)
    }


    Column (
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize()

    ){
        when(cont){
            0 -> idImagen=R.drawable.huerto_vacio
            1 -> idImagen=R.drawable.huerto_cerezo
            2-> idImagen=R.drawable.huerto_cerezas
        }

        mostrarImagen(idImagen)


        Button(
            onClick = {
                cont++
                if (cont>2){
                    cont=0
                }
                if(cont == 2){
                    num = (1..5).random()
                    suma+=num
                }
            }
        )
        {
            Text("Pasar imagen y sumar")
        }
        Text("Suma: $suma")

        Button(onClick = {
            navController.navigate("hola")
        }) {
            Text(text = "Cambiar pagina")

        }
    }


}

@Composable
fun mostrarImagen(idImagen: Int) {
    var imagen = painterResource(id = idImagen)
    Image(
        painter = imagen,
        contentDescription = "huerto" ,
        contentScale = ContentScale.Crop
    )
}

@Composable
fun holaMundo() {
    Text(text = "hola wey")
}

@Composable
fun pruebaNavigation() {
    val navController = rememberNavController()
    NavHost(navController, startDestination = "inicio") {
        composable("hola") { holaMundo() }
        composable("inicio") { Greeting(navController) }
    }
}




@Preview(showBackground = true)
@Composable
fun GreetingPreview1() {
    RegistroTheme {
        pruebaNavigation()
    }
}